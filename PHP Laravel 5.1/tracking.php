<!DOCTYPE html>
<html lang="en">

<head>
    <title>track-trace</title>
    <link rel="alternate" type="text/html" href="http://www.pakkesporing.no/" hreflang="no" lang="no" title="Pakkesporing" />
    <link rel="alternate" media="only screen and (max-width: 640px)" href="http://touch.track-trace.com/">
    <meta name="keywords" content="parcel, tracking, track, shipment, DHL, DHL Worldwide Express, UPS, united parcel service, TNT, FedEx, Federal Express, postal, EMS, air cargo, container" />
    <meta name="description" content="Track parcels/shipments with companies like UPS, DHL, TNT and FedEx. In addition special services for air cargo, containers and post." />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon-precomposed" href="/apple-touch-icon-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/apple-touch-icon-72x72-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/apple-touch-icon-114x114-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/apple-touch-icon-144x144-precomposed.png" />
    <link rel="image_src" href="http://www.track-trace.com/apple-touch-icon-144x144-precomposed.png" />
    <link rel="stylesheet" media="screen" href="http://cf.track-trace.com/assets/application-6d0a8e0a853629d0fb6f3d17a2dc86741dc93c1d948f57002ad24f70521a6410.css" />
    <script src="http://cf.track-trace.com/assets/application-3c26284057a92b0e2648b5d142020bc4815767bbf9fc68bc054ba422eb77e374.js"></script>
    <meta http-equiv="Content-Language" content="en" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />


   

</head>

<body>
    <!-- 2016-09-09 00:45:02 +0000 -->

    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-5">
                    <div class="tt-logo"><img border="0" alt="false" src="http://cf.track-trace.com/assets/logo-91dc705fc3b0e44c4543d03a84d5aed0a75616cd4e392f934d6ad5c7a8ba5d3b.png" /><span class="tt-logo-text">  TESTING </span></div>
                </div>
                

            </div>
        </div>
    </div>

   

    <div class="container">


       

               

                <div class="jq-home-form-area">
                    <div class="row">
                        <div class="home-line">
                            <div class="col-md-4 col-sm-4 home-logo">
                                <strong>DHL</strong>

                                <a href="http://www.dhl.com/">
                                    <div class="logo-dhl"></div>
                                </a>
                              

                            </div>
                            <div class="col-md-8 col-sm-8 home-form jq-form-home-general">
                                <form action="http://www.dhl.com/content/g0/en/express/tracking.shtml" method="get" name="DHL" class="form-track">
                                    <div class="input-group">
                                        <input type="hidden" name="brand" id="brand" value="DHL" />
                                        <input type="text" name="AWB" id="AWB" value="" maxlength="40" class="form-control input-lg" />
                                        <span class="input-group-btn">
<input type="submit" name="commit" value="Track!" class="btn btn-primary btn-lg" />
</span>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="home-line">
                            <div class="col-md-4 col-sm-4 home-logo">
                                <strong>UPS</strong>

                                <a href="http://www.ups.com/">
                                    <div class="logo-ups"></div>
                                </a>

                            </div>
                            <div class="col-md-8 col-sm-8 home-form jq-form-home-general">
                                <form action="http://wwwapps.ups.com/etracking/tracking.cgi" method="get" name="UPS" class="form-track">
                                    <div class="input-group">
                                        <input type="hidden" name="TypeOfInquiryNumber" id="TypeOfInquiryNumber" value="T" />
                                        <input type="text" name="InquiryNumber1" id="InquiryNumber1" value="" maxlength="40" class="form-control input-lg" />
                                        <span class="input-group-btn">
<input type="submit" name="commit" value="Track!" class="btn btn-primary btn-lg" />
</span>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="home-line">
                            <div class="col-md-4 col-sm-4 home-logo">
                                <strong>TNT</strong>

                                <a href="http://www.tnt.com/">
                                    <div class="logo-tnt"></div>
                                </a>

                            </div>
                            <div class="col-md-8 col-sm-8 home-form jq-form-home-general">
                                <form action="https://www.tnt.com/express/en_gc/site/home/applications/tracking.html" method="get" name="TNT" class="form-track">
                                    <div class="input-group">
                                        <input type="hidden" name="respCountry" id="respCountry" value="gc" />
                                        <input type="hidden" name="respLang" id="respLang" value="en" />
                                        <input type="hidden" name="navigation" id="navigation" value="1" />
                                        <input type="hidden" name="page" id="page" value="1" />
                                        <input type="hidden" name="sourceID" id="sourceID" value="1" />
                                        <input type="hidden" name="sourceCountry" id="sourceCountry" value="ww" />
                                        <input type="hidden" name="plazaKey" id="plazaKey" />
                                        <input type="hidden" name="refs" id="refs" />
                                        <input type="hidden" name="requesttype" id="requesttype" value="GEN" />
                                        <input type="text" name="cons" id="cons" value="" maxlength="40" class="form-control input-lg" />
                                        <input type="hidden" name="searchType" id="searchType" value="CON" />
                                        <span class="input-group-btn">
<input type="submit" name="commit" value="Track!" class="btn btn-primary btn-lg" />
</span>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="home-line">
                            <div class="col-md-4 col-sm-4 home-logo">
                                <strong>Fedex</strong>

                                <a href="http://www.fedex.com/">
                                    <div class="logo-blank"></div>
                                </a>

                            </div>
                            <div class="col-md-8 col-sm-8 home-form jq-form-home-general">
                                <form action="https://www.fedex.com/fedextrack/" method="get" name="FEDEX" class="form-track">
                                    <div class="input-group">
                                        <input type="text" name="tracknumbers" id="tracknumbers" value="" maxlength="40" class="form-control input-lg" />
                                        <span class="input-group-btn">
<input type="submit" name="commit" value="Track!" class="btn btn-primary btn-lg" />
</span>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="home-line">
                            <div class="col-md-4 col-sm-4 home-logo">
                                <strong>DB Schenker USA</strong>

                                <a href="http://www.dbschenkerusa.com/">
                                    <div class="logo-schenker"></div>
                                </a>

                            </div>
                            <div class="col-md-8 col-sm-8 home-form jq-form-home-general">
                                <form action="http://apps.dbschenkerusa.com/apps/Tracking/SchenkerDetail.aspx" method="get" name="BAX" class="form-track">
                                    <div class="input-group">
                                        <input type="hidden" name="rt" id="rt" value="ff" />
                                        <input type="text" name="rn" id="rn" value="" maxlength="40" class="form-control input-lg" />
                                        <input type="hidden" name="rtn" id="rtn" value="Schenker%20STT%20No." />
                                        <span class="input-group-btn">
<input type="submit" name="commit" value="Track!" class="btn btn-primary btn-lg" />
</span>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="home-line">
                            <div class="col-md-4 col-sm-4 home-logo">
                                <strong>DHL G. F.</strong>

                                <a href="http://www.dhl.com/">
                                    <div class="logo-dhl"></div>
                                </a>

                            </div>
                            <div class="col-md-8 col-sm-8 home-form jq-form-home-general">
                                <form action="https://dhli.dhl.com/dhli-client/publicTracking" method="get" name="AEI" class="form-track">
                                    <div class="input-group">
                                        <input type="hidden" name="searchType" id="searchType" value="HBN" />
                                        <input type="text" name="searchValue" id="searchValue" value="" maxlength="40" class="form-control input-lg" />
                                        <span class="input-group-btn">
<input type="submit" name="commit" value="Track!" class="btn btn-primary btn-lg" />
</span>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="home-line">
                            <div class="col-md-4 col-sm-4 home-logo">
                                <strong>Air Cargo</strong>

                                <a href="/aircargo">
                                    <div class="logo-cargo"></div>
                                </a>

                            </div>
                            <div class="col-md-8 col-sm-8 home-form jq-form-home-multi">
                                <form action="/aircargo" method="post" id="aircargoform" name="aircargoform" target="_top" class="form-track">
                                    <div class="input-group">
                                        <input type="text" name="number" id="number" maxlength="40" class="form-control input-lg" />
                                        <input type="hidden" name="page" id="page" value="main" />
                                        <input type="hidden" name="options" id="options" value="" />
                                        <!-- Want to put this tracking on your website/intranet? Please use https://connect.track-trace.com/ -->
                                        <input type="hidden" name="v" id="v" value="R" />
                                        <span class="input-group-btn">
  <input type="submit" name="commit" value="Track!" class="btn btn-primary btn-lg" />
  </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="home-line">
                            <div class="col-md-4 col-sm-4 home-logo">
                                <strong>Post/EMS(with USPS)</strong>

                                <a href="/post">
                                    <div class="logo-post"></div>
                                </a>

                            </div>
                            <div class="col-md-8 col-sm-8 home-form jq-form-home-multi">
                                <form action="/post" method="post" id="postform" name="postform" target="_top" class="form-track">
                                    <div class="input-group">
                                        <input type="text" name="number" id="number" maxlength="40" class="form-control input-lg" />
                                        <input type="hidden" name="page" id="page" value="main" />
                                        <input type="hidden" name="options" id="options" value="" />
                                        <!-- Want to put this tracking on your website/intranet? Please use https://connect.track-trace.com/ -->
                                        <input type="hidden" name="v" id="v" value="R" />
                                        <span class="input-group-btn">
  <input type="submit" name="commit" value="Track!" class="btn btn-primary btn-lg" />
  </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="home-line">
                            <div class="col-md-4 col-sm-4 home-logo">
                                <strong>Container</strong>

                                <a href="/container">
                                    <div class="logo-container"></div>
                                </a>

                            </div>
                            <div class="col-md-8 col-sm-8 home-form jq-form-home-multi">
                                <form action="/container" method="post" id="containerform" name="containerform" target="_top" class="form-track">
                                    <div class="input-group">
                                        <input type="text" name="number" id="number" maxlength="40" class="form-control input-lg" />
                                        <input type="hidden" name="page" id="page" value="main" />
                                        <input type="hidden" name="options" id="options" value="" />
                                        <!-- Want to put this tracking on your website/intranet? Please use https://connect.track-trace.com/ -->
                                        <input type="hidden" name="v" id="v" value="R" />
                                        <span class="input-group-btn">
  <input type="submit" name="commit" value="Track!" class="btn btn-primary btn-lg" />
  </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="home-line">
                            <div class="col-md-4 col-sm-4 home-logo">
                                <strong>Bill Of Lading</strong>

                                <a href="/bol">
                                    <div class="logo-bol"></div>
                                </a>

                            </div>
                            <div class="col-md-8 col-sm-8 home-form jq-form-home-multi">
                                <form action="/bol" method="post" id="bolform" name="bolform" target="_top" class="form-track">
                                    <div class="input-group">
                                        <input type="text" name="number" id="number" maxlength="40" class="form-control input-lg" />
                                        <input type="hidden" name="page" id="page" value="main" />
                                        <input type="hidden" name="options" id="options" value="" />
                                        <!-- Want to put this tracking on your website/intranet? Please use https://connect.track-trace.com/ -->
                                        <input type="hidden" name="v" id="v" value="R" />
                                        <span class="input-group-btn">
  <input type="submit" name="commit" value="Track!" class="btn btn-primary btn-lg" />
  </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>


                </div>

            </div>

            <div class="col-md-4">

                <div id="r-ad-tag" class="hidden-sm hidden-xs">
                    &#9666;advertisement&#9656;
                </div>
                <div id="r-ad">
                    <style type="text/css">
                        .adslot_1 {
                            display: block;
                        }
                        
                        @media (max-width: 992px) {
                            .adslot_1 {
                                display: none;
                            }
                        }
                    </style>
                    <!-- track_right_tall_R -->
                  

        

        

      

    </div>
    <div class="container">
        <footer>

            <span style="float:right">TESTING</span>

          

        </footer>

    </div>

</body>

</html>